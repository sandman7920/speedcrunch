# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2022-12-23 20:56+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: extensions/speedcrunch_domain.py:86
msgid "Parameters"
msgstr ""

#: extensions/speedcrunch_domain.py:89
msgid "Returns"
msgstr ""

#: extensions/speedcrunch_domain.py:149
msgid "%s() (function)"
msgstr ""

#: extensions/speedcrunch_domain.py:157
msgid "%s (constant)"
msgstr ""

#: extensions/speedcrunch_domain.py:165
msgid "Function Index"
msgstr ""

#: extensions/speedcrunch_domain.py:167
msgid "functions"
msgstr ""

#: extensions/speedcrunch_domain.py:190
msgid "function"
msgstr ""

#: extensions/speedcrunch_domain.py:192
msgid "constant"
msgstr ""

